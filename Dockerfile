FROM openjdk:15-slim

ENV SPRING_PROFILE="default"

VOLUME /tmp

ADD /target/*.jar connect-services-admin-server.jar
ENTRYPOINT echo Europe/Zagreb > /etc/timezone && \
	java -Djava.security.egd=file:/dev/./urandom -jar -Dspring.profiles.active=$SPRING_PROFILE /connect-services-admin-server.jar
